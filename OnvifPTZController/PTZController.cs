﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using onvif.OnvifMedia10;
using onvif.OnvifPTZService;
using System.Threading;

namespace onvif
{
    public class PTZposition
    {
        public float pan;
        public float tilt;
        public float zoom;
        public PTZposition() { }
        public PTZposition(float p, float t, float z) { pan = p; tilt = t; zoom = z;}
    }

    public class PTZController
    {
        private MediaClient mediaClient;
        private PTZClient ptzClient;
        private Profile profile;
        private OnvifPTZService.PTZSpeed velocity;
        private PTZVector vector;
        private PTZConfigurationOptions options;
        private enum Direction { None, Up, Down, Left, Right };
        //private Direction direction;

        

        public PTZController() { }

        public PTZController(String ip, String userName, String password)
        {
            Connect(ip,userName,password);
        }

        public bool Connect(String ip, String userName, String password)
        {
            var messageElement = new TextMessageEncodingBindingElement()
            {
                MessageVersion = MessageVersion.CreateVersion(EnvelopeVersion.Soap12, AddressingVersion.None)
            };
            HttpTransportBindingElement httpBinding = new HttpTransportBindingElement()
            {
                AuthenticationScheme = AuthenticationSchemes.Digest
            };
            CustomBinding bind = new CustomBinding(messageElement, httpBinding);
            mediaClient = new MediaClient(bind, new EndpointAddress($"http://" + ip + "/onvif/device_service"));
            mediaClient.ClientCredentials.HttpDigest.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            mediaClient.ClientCredentials.HttpDigest.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            mediaClient.ClientCredentials.HttpDigest.ClientCredential.UserName = userName;
            mediaClient.ClientCredentials.HttpDigest.ClientCredential.Password = password;
            ptzClient = new PTZClient(bind, new EndpointAddress($"http://" + ip + "/onvif/device_service"));
            ptzClient.ClientCredentials.HttpDigest.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            ptzClient.ClientCredentials.HttpDigest.ClientCredential.UserName = userName;
            ptzClient.ClientCredentials.HttpDigest.ClientCredential.Password = password;

            var profs = mediaClient.GetProfiles();
            profile = mediaClient.GetProfile(profs[0].token);
            System.Console.WriteLine(profile.token);
            var configs = ptzClient.GetConfigurations();
            options = ptzClient.GetConfigurationOptions(configs[0].token);

            velocity = new OnvifPTZService.PTZSpeed()
            {
                PanTilt = new OnvifPTZService.Vector2D()
                {
                    x = 0,
                    y = 0,
                    space = options.Spaces.ContinuousPanTiltVelocitySpace[0].URI,                    
                },
                Zoom = new OnvifPTZService.Vector1D()
                {
                    x = 0,
                    space = options.Spaces.ContinuousZoomVelocitySpace[0].URI,
                }
            };
            //OnvifPTZService.Space2DDescription s2desc = new OnvifPTZService.Space2DDescription();
            vector = new PTZVector()
            {
                PanTilt = new OnvifPTZService.Vector2D()
                {
                    x = 0.0f,
                    y = 0.0f,
                    space = options.Spaces.AbsolutePanTiltPositionSpace[0].URI
                }
            };
            
            //ptzClient.GotoHomePosition(profile.token, velocity);
            //OnvifPTZService.PTZConfiguration[] config = ptzClient.GetCompatibleConfigurations(profile.token);
            //Console.WriteLine("Absolute pantilt  position space:");
            //Console.WriteLine(config[0].DefaultAbsolutePantTiltPositionSpace);
            //Console.WriteLine("Absolute zoom position space:");
            //Console.WriteLine(config[0].DefaultAbsoluteZoomPositionSpace);
            //Console.WriteLine("Default ptz speed");
            ////Console.WriteLine(config[0]);
            //Console.WriteLine("Zoom limits");
            ////Console.WriteLine(config[0].ZoomLimits.Range.XRange.Max);
            //Console.WriteLine(options.Spaces.AbsolutePanTiltPositionSpace[0].URI);
            return true;
        }

        public void MoveAbsolute(float speed, float pan, float tilt)
        {
            System.Console.WriteLine("Move absolute");
            velocity.PanTilt.x = speed;
            velocity.PanTilt.y = speed;             
            vector.PanTilt.x = pan;
            vector.PanTilt.x = tilt;
            ptzClient.AbsoluteMove(profile.token, vector, velocity);
        }

        public void MoveAbsolute(float speed, float pan, float tilt, int timeout_ms)
        {
            System.Console.WriteLine("Move absolute and wait");
            velocity.PanTilt.x = speed;
            velocity.PanTilt.y = speed;
            vector.PanTilt.x = pan;
            vector.PanTilt.x = tilt;
            ptzClient.AbsoluteMove(profile.token, vector, velocity);
            for(int i = 0; i < timeout_ms; i++)
            {
                PTZStatus status = ptzClient.GetStatus(profile.token);
                if (Math.Abs(status.Position.PanTilt.x - pan) < 0.05 || Math.Abs(status.Position.PanTilt.y - tilt) < 0.05)
                {
                    break;
                }
                Thread.Sleep(100);
            }
        }

        public void MoveAbsolute(float speed, float pan, float tilt, float zoom)
        {
            System.Console.WriteLine("Move absolute with zoom");
            
            OnvifPTZService.PTZSpeed v;
            v = new OnvifPTZService.PTZSpeed()
            {
                PanTilt = new OnvifPTZService.Vector2D()
                {
                    x = speed,
                    y = speed,
                    space = options.Spaces.PanTiltSpeedSpace[0].URI,
                },
                Zoom = new OnvifPTZService.Vector1D()
                {
                    x = 0,
                    space = options.Spaces.ZoomSpeedSpace[0].URI,
                }
            };
            Console.WriteLine("Max Zoom = ");
            Console.WriteLine(options.Spaces.AbsoluteZoomPositionSpace[0].XRange.Max);
            Console.WriteLine("Min Zoom = ");
            Console.WriteLine(options.Spaces.AbsoluteZoomPositionSpace[0].XRange.Min);
            Console.WriteLine("Max Speed = ");
            Console.WriteLine(options.Spaces.AbsolutePanTiltPositionSpace[0].XRange.Max);
            Console.WriteLine("Min Speed = ");
            Console.WriteLine(options.Spaces.AbsolutePanTiltPositionSpace[0].XRange.Min);
            vector = new PTZVector()
            {
                PanTilt = new OnvifPTZService.Vector2D()
                {
                    x = pan,
                    y = tilt,
                    space = options.Spaces.AbsolutePanTiltPositionSpace[0].URI
                },
                Zoom = new OnvifPTZService.Vector1D()
                {
                    x = zoom,
                    space = options.Spaces.AbsoluteZoomPositionSpace[0].URI
                }
            };
            ptzClient.AbsoluteMove(profile.token, vector, velocity);
        }

        public void MoveAbsolute(float speed, float pan, float tilt, float zoom, int timeout_ms)
        {
            System.Console.WriteLine("Move absolute with zoom and wait");
            velocity.PanTilt.x = speed;
            velocity.PanTilt.y = speed;
            vector.PanTilt.x = pan;
            vector.PanTilt.x = tilt;
            vector.Zoom.x = zoom;
            ptzClient.AbsoluteMove(profile.token, vector, velocity);
            for (int i = 0; i < timeout_ms; i++)
            {
                PTZStatus status = ptzClient.GetStatus(profile.token);
                if (Math.Abs(status.Position.PanTilt.x - pan) < 0.05 || Math.Abs(status.Position.PanTilt.y - tilt) < 0.05 || Math.Abs(status.Position.Zoom.x - zoom) < 0.05)
                {
                    break;
                }
                Thread.Sleep(100);
            }
        }

        public void MoveRight(float speed)
        {
            velocity.PanTilt.x = speed;
            velocity.PanTilt.y = 0;
            //direction = Direction.Right;
            ptzClient.ContinuousMove(profile.token, velocity, "PT10S");
        }

        public async Task MoveRightAsync(float speed)
        {
            velocity.PanTilt.x = speed;
            velocity.PanTilt.y = 0;
            //direction = Direction.Right;
            await ptzClient.ContinuousMoveAsync(profile.token, velocity, "PT10S");
        }

        public void MoveLeft(float speed)
        {
            velocity.PanTilt.x = -speed;
            velocity.PanTilt.y = 0;
            //direction = Direction.Left;
            ptzClient.ContinuousMove(profile.token, velocity, "PT10S");
        }

        public async Task MoveLeftAsync(float speed)
        {
            velocity.PanTilt.x = -speed;
            velocity.PanTilt.y = 0;
            //direction = Direction.Left;
            await ptzClient.ContinuousMoveAsync(profile.token, velocity, "PT10S");
        }

        public void MoveUp(float speed)
        {
            velocity.PanTilt.x = 0;
            velocity.PanTilt.y = speed;
            ptzClient.ContinuousMove(profile.token, velocity, "PT10S");
        }

        public void MoveDown(float speed)
        {
            velocity.PanTilt.x = 0;
            velocity.PanTilt.y = -speed;
            ptzClient.ContinuousMove(profile.token, velocity, "PT10S");
        }

        public void MoveStop()
        {
            ptzClient.Stop(profile.token, true, true);
        }

        public void RequestPanTilt(float pan, float tilt)
        {
            PTZStatus status = ptzClient.GetStatus(profile.token);
            pan = status.Position.PanTilt.x;
            tilt = status.Position.PanTilt.y;
        }

        public float RequestZoom()
        {
            PTZStatus status = ptzClient.GetStatus(profile.token);
            return status.Position.Zoom.x;
        }

        public PTZposition RequestPosition()
        {
            PTZposition p = new PTZposition();
            PTZStatus status = ptzClient.GetStatus(profile.token);
            p.pan = status.Position.PanTilt.x;
            p.tilt = status.Position.PanTilt.y;
            p.zoom = status.Position.Zoom.x;
            return p;
        }

        public void SetAbsoluteZoom(float zoom)
        {
            PTZStatus status = ptzClient.GetStatus(profile.token);
        }

        public void Dispose()
        {
            mediaClient.Close();
            ptzClient.Close();
        }



       
    }
}
