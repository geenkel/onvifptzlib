﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Timers;
using System.Windows.Forms;
using onvifPTZ.OnvifMedia10;
using onvifPTZ.OnvifPTZService;
using System.Threading;
using onvif;

namespace onvifPTZ
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        private enum Direction { None, Up, Down, Left, Right };
        [STAThread]
        static void Main()
        {
            PTZController cameraPtz = new PTZController();
            cameraPtz.Connect("192.168.0.27", "admin", "123456");
            cameraPtz.MoveAbsolute(0.1f, 0.5f, 0.5f, 0f);
            //cameraPtz.MoveDown(0.5f);
            //Thread.Sleep(2000);
            //cameraPtz.MoveStop();
            

            
            //while (true) { } ;
            //MediaClient mediaClient;
            //PTZClient ptzClient;
            //Profile profile;
            //OnvifPTZService.PTZSpeed velocity;
            //PTZVector vector;
            //PTZConfigurationOptions options;
            //Direction direction;
            //var messageElement = new TextMessageEncodingBindingElement()
            //{
            //    MessageVersion = MessageVersion.CreateVersion(EnvelopeVersion.Soap12, AddressingVersion.None)
            //};
            //HttpTransportBindingElement httpBinding = new HttpTransportBindingElement()
            //{
            //    AuthenticationScheme = AuthenticationSchemes.Digest
            //};
            //CustomBinding bind = new CustomBinding(messageElement, httpBinding);
            //mediaClient = new MediaClient(bind, new EndpointAddress($"http://192.168.0.27/onvif/device_service"));
            //mediaClient.ClientCredentials.HttpDigest.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            //mediaClient.ClientCredentials.HttpDigest.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            //mediaClient.ClientCredentials.HttpDigest.ClientCredential.UserName = "admin";
            //mediaClient.ClientCredentials.HttpDigest.ClientCredential.Password = "123456";
            //ptzClient = new PTZClient(bind, new EndpointAddress($"http://192.168.0.27/onvif/device_service"));
            //ptzClient.ClientCredentials.HttpDigest.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            //ptzClient.ClientCredentials.HttpDigest.ClientCredential.UserName = "admin";
            //ptzClient.ClientCredentials.HttpDigest.ClientCredential.Password = "123456";

            //var profs = mediaClient.GetProfiles();
            //profile = mediaClient.GetProfile(profs[0].token);
            //System.Console.WriteLine(profile.token);
            //var configs = ptzClient.GetConfigurations();
            //options = ptzClient.GetConfigurationOptions(configs[0].token);
            ////System.Console.WriteLine(options.)

            //if (options != null)
            //{
            //    System.Console.WriteLine(options.Spaces.ContinuousPanTiltVelocitySpace[0].URI);
            //}


            //velocity = new OnvifPTZService.PTZSpeed()
            //{
            //    PanTilt = new OnvifPTZService.Vector2D()
            //    {
            //        x = 0,
            //        y = 0,
            //        space = options.Spaces.ContinuousPanTiltVelocitySpace[0].URI,
            //    },
            //    Zoom = new OnvifPTZService.Vector1D()
            //    {
            //        x = 0,
            //        space = options.Spaces.ContinuousZoomVelocitySpace[0].URI,
            //    }
            //};

            ////OnvifPTZService.Space2DDescription s2desc = new OnvifPTZService.Space2DDescription();

            
            //vector = new PTZVector()
            //{
            //    PanTilt = new OnvifPTZService.Vector2D()
            //    {
            //        x = 0,
            //        y = 0,
            //        space = "http://www.onvif.org/ver10/tptz/PanTiltSpaces/TranslationGenericSpac",
            //    }
            //};
            
            //velocity.PanTilt.x = options.Spaces.ContinuousPanTiltVelocitySpace[0].XRange.Max;
            //velocity.PanTilt.y = 0;
            ////ptzClient.ContinuousMoveAsync(profile.token, velocity, "PT10S");
            
            //Thread.Sleep(2000);
            //direction = Direction.None;
            //ptzClient.Stop(profile.token, true, true);

            //Thread.Sleep(2000);
            //PTZVector vector3;
            //vector3 = new PTZVector()
            //{
            //    PanTilt = new OnvifPTZService.Vector2D()
            //    {
            //        x = 0.1f,
            //        y = 0.2f,
            //        space = options.Spaces.AbsolutePanTiltPositionSpace[0].URI
            //    },
            //    Zoom = new OnvifPTZService.Vector1D()
            //    {
            //        x = 0,
            //        space = options.Spaces.AbsoluteZoomPositionSpace[0].URI
            //    }
                
            //};

            //Console.WriteLine(options.Spaces.AbsolutePanTiltPositionSpace[0].XRange.Max);
            //ptzClient.AbsoluteMove(profile.token, vector3, velocity);
            //PTZStatus status;
            //for(int i = 0; i < 50; i++)
            //{
            //    status = ptzClient.GetStatus(profile.token);               
            //    System.Console.WriteLine("------------------");
            //    System.Console.WriteLine(vector3.PanTilt.x);
            //    System.Console.WriteLine(vector3.PanTilt.y);
            //    System.Console.WriteLine(vector3.Zoom.x);
            //}

            ////direction = Direction.None;
            ////ptzClient.Stop(profile.token, true, true);
            
            //System.Console.WriteLine("hecho");
            //System.Console.WriteLine("Zoom Max:");
            //System.Console.WriteLine(options.Spaces.AbsoluteZoomPositionSpace[0].XRange.Max);
            //System.Console.WriteLine("Zoom Min:");
            //System.Console.WriteLine(options.Spaces.AbsoluteZoomPositionSpace[0].XRange.Min);

            //status = ptzClient.GetStatus(profile.token);
            //vector3.PanTilt.x =0.4f;
            //vector3 = status.Position;
            
            //System.Console.WriteLine(vector3.PanTilt.x);
            //System.Console.WriteLine(vector3.Zoom.x);





            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
